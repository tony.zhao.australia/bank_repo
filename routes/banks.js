let express = require('express');
let router = express.Router();
const Joi = require('joi');
let bankmodel = require('../models/bank');

// GET accounts list.
router.get('/accounts/:user_id', async function(req, res, next) {
    let user_id = req.params.user_id

    //validate the input user_id
    const schema = Joi.object({
        user_id: Joi.number()
            .integer()
            .min(1)
            .max(9999999999)
            .required(),
    })

    const { error, value } = schema.validate({user_id});

    if(error){
        res.send({
            success:"no",
            error: error.details[0].message
        })
        return
    }

    //query the database to get the account list for this user
    let selectRes = []
    try{
        selectRes = await bankmodel.getAccountList(user_id)
    }catch (e) {
        //todo  add this error into log

        res.send({
            success:"no",
            error: 'An error when we query the account list'
        })
        return
    }

    if(selectRes.length > 0){
        res.send({
            success:"yes",
            data: selectRes
        })
    }else{
        res.send({
            success:"no",
            error: 'we can not get the account list'
        })
    }

    return
});

// GET transaction detail.
router.get('/transactions/:account_id', async function(req, res, next) {
    let account_id = req.params.account_id

    //validate the input user_id
    const schema = Joi.object({
        account_id: Joi.number()
            .integer()
            .min(1)
            .max(9999999999)
            .required(),
    })

    const { error, value } = schema.validate({account_id});

    if(error){
        res.send({
            success:"no",
            error: error.details[0].message
        })
        return
    }

    //query the database to get the transaction for this account_id
    let selectRes = []
    try{
        selectRes = await bankmodel.getAccountTransactions(account_id)
    }catch (e) {
        //todo  add this error into log

        res.send({
            success:"no",
            error: 'An error when we query the account transaction'
        })
        return
    }

    if(selectRes.length > 0){
        res.send({
            success:"yes",
            data: selectRes
        })
    }else{
        res.send({
            success:"no",
            error: 'we can not get the account transaction'
        })
    }
    return
});
module.exports = router;
