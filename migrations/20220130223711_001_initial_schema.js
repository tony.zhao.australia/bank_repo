/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function(knex) {
    await knex.raw("CREATE TABLE IF NOT EXISTS `account_types` (\n" +
        " `id` tinyint unsigned NOT NULL AUTO_INCREMENT,\n" +
        " `name` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Savinds',\n" +
        " `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " PRIMARY KEY (`id`)\n" +
        ") ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci")

    await knex.raw("CREATE TABLE IF NOT EXISTS `currencies` (\n" +
        " `id` smallint unsigned NOT NULL AUTO_INCREMENT,\n" +
        " `name` varchar(11) COLLATE utf8mb4_general_ci NOT NULL,\n" +
        " `country` varchar(33) COLLATE utf8mb4_general_ci NOT NULL,\n" +
        " `ratio_to_usd` decimal(12,2) NOT NULL,\n" +
        " `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " PRIMARY KEY (`id`)\n" +
        ") ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci")

    await knex.raw("CREATE TABLE IF NOT EXISTS `transaction_types` (\n" +
        " `id` tinyint unsigned NOT NULL AUTO_INCREMENT,\n" +
        " `name` varchar(22) COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Credit',\n" +
        " `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " PRIMARY KEY (`id`)\n" +
        ") ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci")

    await knex.raw("CREATE TABLE IF NOT EXISTS `users` (\n" +
        " `id` int unsigned NOT NULL AUTO_INCREMENT,\n" +
        " `email` varchar(33) COLLATE utf8mb4_general_ci DEFAULT NULL,\n" +
        " `password` varchar(33) COLLATE utf8mb4_general_ci DEFAULT NULL,\n" +
        " `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " PRIMARY KEY (`id`)\n" +
        ") ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci")

    await knex.raw("CREATE TABLE IF NOT EXISTS `bank_accounts` (\n" +
        " `id` int unsigned NOT NULL AUTO_INCREMENT,\n" +
        " `account_number` int unsigned DEFAULT NULL,\n" +
        " `account_name` varchar(22) COLLATE utf8mb4_general_ci DEFAULT NULL,\n" +
        " `account_type_id` tinyint unsigned NOT NULL DEFAULT '1',\n" +
        " `balance_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " `opening_available_balance` decimal(12,2) unsigned NOT NULL,\n" +
        " `currency_id` smallint unsigned NOT NULL DEFAULT '1',\n" +
        " `user_id` int unsigned NOT NULL,\n" +
        " `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " PRIMARY KEY (`id`),\n" +
        " KEY `fk_account_type_id` (`account_type_id`),\n" +
        " KEY `fk_currency_id` (`currency_id`),\n" +
        " KEY `fk_user_id` (`user_id`),\n" +
        " CONSTRAINT `fk_account_type_id` FOREIGN KEY (`account_type_id`) REFERENCES `account_types` (`id`),\n" +
        " CONSTRAINT `fk_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`),\n" +
        " CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)\n" +
        ") ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci")

    await knex.raw("CREATE TABLE IF NOT EXISTS `account_transactions` (\n" +
        " `id` int unsigned NOT NULL AUTO_INCREMENT,\n" +
        " `value_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " `transaction_type_id` tinyint unsigned NOT NULL DEFAULT '1',\n" +
        " `transaction_amount` decimal(12,2) unsigned NOT NULL,\n" +
        " `account_id` int unsigned NOT NULL,\n" +
        " `transaction_narrative` text COLLATE utf8mb4_general_ci NOT NULL,\n" +
        " `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
        " PRIMARY KEY (`id`),\n" +
        " KEY `fk_transaction_type_id` (`transaction_type_id`),\n" +
        " KEY `fk_account_id` (`account_id`),\n" +
        " CONSTRAINT `fk_account_id` FOREIGN KEY (`account_id`) REFERENCES `bank_accounts` (`id`),\n" +
        " CONSTRAINT `fk_transaction_type_id` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_types` (`id`)\n" +
        ") ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci")

};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function(knex) {
    await knex.raw("DROP TABLE IF EXISTS bank_accounts, account_transactions, account_types, currencies, transaction_types, users")
};
