-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 31, 2022 at 09:44 AM
-- Server version: 8.0.27-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_transactions`
--

CREATE TABLE `account_transactions` (
  `id` int UNSIGNED NOT NULL,
  `value_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `transaction_type_id` tinyint UNSIGNED NOT NULL DEFAULT '1',
  `transaction_amount` decimal(12,2) UNSIGNED NOT NULL,
  `account_id` int UNSIGNED NOT NULL,
  `transaction_narrative` text COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `account_transactions`
--

INSERT INTO `account_transactions` (`id`, `value_date`, `transaction_type_id`, `transaction_amount`, `account_id`, `transaction_narrative`, `created_at`, `updated_at`) VALUES
(1, '2022-01-31 07:02:46', 1, '1122.23', 1, 'This is international transaction', '2022-01-31 07:02:46', '2022-01-31 07:02:46'),
(2, '2022-01-31 07:02:46', 2, '33.98', 1, 'This is national transaction', '2022-01-31 07:02:46', '2022-01-31 07:02:46'),
(3, '2022-01-31 07:02:46', 1, '1122.23', 2, 'get money from other', '2022-01-31 07:02:46', '2022-01-31 07:02:46'),
(4, '2022-01-31 07:02:46', 2, '1122.23', 2, 'pay for a bill', '2022-01-31 07:02:46', '2022-01-31 07:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `account_types`
--

CREATE TABLE `account_types` (
  `id` tinyint UNSIGNED NOT NULL,
  `name` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Savinds',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `account_types`
--

INSERT INTO `account_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Savings', '2022-01-29 13:00:51', '2022-01-29 13:00:51'),
(2, 'Current', '2022-01-29 13:00:51', '2022-01-29 13:00:51');

-- --------------------------------------------------------

--
-- Table structure for table `bank_accounts`
--

CREATE TABLE `bank_accounts` (
  `id` int UNSIGNED NOT NULL,
  `account_number` int UNSIGNED DEFAULT NULL,
  `account_name` varchar(22) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `account_type_id` tinyint UNSIGNED NOT NULL DEFAULT '1',
  `balance_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `opening_available_balance` decimal(12,2) UNSIGNED NOT NULL,
  `currency_id` smallint UNSIGNED NOT NULL DEFAULT '1',
  `user_id` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `bank_accounts`
--

INSERT INTO `bank_accounts` (`id`, `account_number`, `account_name`, `account_type_id`, `balance_date`, `opening_available_balance`, `currency_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 111111, 'AUSavings111', 1, '2022-01-30 08:31:30', '1248397.22', 1, 1, '2022-01-30 08:31:30', '2022-01-30 08:31:30'),
(2, 222222, 'SGCurrent222', 2, '2022-01-30 08:31:30', '1123.22', 2, 1, '2022-01-30 08:31:30', '2022-01-30 08:31:30'),
(3, 333333, 'SGCurrent333', 2, '2022-01-30 08:31:30', '6723.22', 2, 2, '2022-01-30 08:31:30', '2022-01-30 08:31:30'),
(4, 444444, 'AUSavings444', 1, '2022-01-30 08:31:30', '1248397.22', 1, 2, '2022-01-30 08:31:30', '2022-01-30 08:31:30');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` smallint UNSIGNED NOT NULL,
  `name` varchar(11) COLLATE utf8mb4_general_ci NOT NULL,
  `country` varchar(33) COLLATE utf8mb4_general_ci NOT NULL,
  `ratio_to_usd` decimal(12,2) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `country`, `ratio_to_usd`, `created_at`, `updated_at`) VALUES
(1, 'AUD', 'Australia', '1.35', '2022-01-30 08:30:10', '2022-01-30 08:30:10'),
(2, 'SGD', 'Singapore', '1.45', '2022-01-30 08:30:10', '2022-01-30 08:30:10');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_types`
--

CREATE TABLE `transaction_types` (
  `id` tinyint UNSIGNED NOT NULL,
  `name` varchar(22) COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Credit',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `transaction_types`
--

INSERT INTO `transaction_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Credit', '2022-01-29 13:00:51', '2022-01-29 13:00:51'),
(2, 'Debit', '2022-01-29 13:00:51', '2022-01-29 13:00:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `email` varchar(33) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(33) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'tonyzhao@gmail.com', '$klsjdjkjkskdj', '2022-01-30 08:24:57', '2022-01-30 08:24:57'),
(2, 'jacksmith@gmail.com', '$123sdadasd', '2022-01-30 08:24:57', '2022-01-30 08:24:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_transactions`
--
ALTER TABLE `account_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transaction_type_id` (`transaction_type_id`),
  ADD KEY `fk_account_id` (`account_id`);

--
-- Indexes for table `account_types`
--
ALTER TABLE `account_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_accounts`
--
ALTER TABLE `bank_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_account_type_id` (`account_type_id`),
  ADD KEY `fk_currency_id` (`currency_id`),
  ADD KEY `fk_user_id` (`user_id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_types`
--
ALTER TABLE `transaction_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_transactions`
--
ALTER TABLE `account_transactions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `account_types`
--
ALTER TABLE `account_types`
  MODIFY `id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bank_accounts`
--
ALTER TABLE `bank_accounts`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` smallint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaction_types`
--
ALTER TABLE `transaction_types`
  MODIFY `id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account_transactions`
--
ALTER TABLE `account_transactions`
  ADD CONSTRAINT `fk_account_id` FOREIGN KEY (`account_id`) REFERENCES `bank_accounts` (`id`),
  ADD CONSTRAINT `fk_transaction_type_id` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_types` (`id`);

--
-- Constraints for table `bank_accounts`
--
ALTER TABLE `bank_accounts`
  ADD CONSTRAINT `fk_account_type_id` FOREIGN KEY (`account_type_id`) REFERENCES `account_types` (`id`),
  ADD CONSTRAINT `fk_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`),
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
