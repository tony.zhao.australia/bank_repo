## 1. Introduction
This repo is developed with Node Express and MySQL.

## 2. Important packages
### (1) joi
joi is used to validate user input

### (2) knex
knex is used for DB query builder and DB migration

## 3. Setup local environment
### (1) $npm install
### (2) $mv config/config.json.example config/config.json
### (3) setup the DB connection information in the config.json
### (4) $npm install knex -g
### (5) $knex migrate:up  
to setup the DB tables. Or you can import the shared_files/bank_db.sql which includes DB structure and data
### (6) npm start

## 4. Endpoints
### (1) accounts list
http://localhost:3000/banks/accounts/:user_id

example:
http://localhost:3000/banks/accounts/1
###  response:
{
"success": "yes",
"data": [
{
"id": 1,
"account_number": 111111,
"account_name": "AUSavings111",
"balance_date": "2022-01-29T21:31:30.000Z",
"opening_available_balance": 1248397.22,
"account_type": "Savings",
"currency": "AUD"
},
{
"id": 2,
"account_number": 222222,
"account_name": "SGCurrent222",
"balance_date": "2022-01-29T21:31:30.000Z",
"opening_available_balance": 1123.22,
"account_type": "Current",
"currency": "SGD"
}
]
}
### (2) transactions
http://localhost:3000/banks/transactions/:account_id

example
http://localhost:3000/banks/transactions/1

###  response:
{
"success": "yes",
"data": [
{
"id": 1,
"account_number": 111111,
"account_name": "AUSavings111",
"value_date": "2022-01-30T20:02:46.000Z",
"transaction_amount": 1122.23,
"transaction_narrative": "This is international transaction",
"transaction_type": "Credit",
"currency": "AUD"
},
{
"id": 2,
"account_number": 111111,
"account_name": "AUSavings111",
"value_date": "2022-01-30T20:02:46.000Z",
"transaction_amount": 33.98,
"transaction_narrative": "This is national transaction",
"transaction_type": "Debit",
"currency": "AUD"
}
]
}

## 5. Todo list
### (1) add the exception messages into log
### (2) CI/CD
### (3) Unit tests and feature tests

