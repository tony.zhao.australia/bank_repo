// Update with your config settings.
const config = require('./config/config.json');

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {

  development: {
    client: 'mysql',
    connection: config[config["current_env"]]["db_info"]
  },

  staging: {
    client: 'mysql',
    connection: config[config["current_env"]]["db_info"]
  },

  production: {
    client: 'mysql',
    connection: config[config["current_env"]]["db_info"]
  }

};
