const config = require('../config/config.json');

const knex = require('knex')({
    client: 'mysql',
    connection: config[config["current_env"]]["db_info"]
});

async function getAccountList(user_id){
    let selectRes = await knex('bank_accounts')
        .join('account_types', 'bank_accounts.account_type_id', 'account_types.id')
        .join('currencies', 'bank_accounts.currency_id', 'currencies.id')
        .join('users', 'bank_accounts.user_id', 'users.id')
        .where('bank_accounts.user_id', user_id)
        .select('bank_accounts.id', 'account_number', 'account_name', 'balance_date', 'opening_available_balance',
            'account_types.name as account_type', 'currencies.name as currency',
        )

    return selectRes
}

async function getAccountTransactions(account_id){
    let selectRes = await knex('account_transactions')
        .join('bank_accounts', 'bank_accounts.id', 'account_transactions.account_id')
        .join('currencies', 'bank_accounts.currency_id', 'currencies.id')
        .join('transaction_types', 'account_transactions.transaction_type_id', 'transaction_types.id')
        .where('account_transactions.account_id', account_id)
        .select('account_transactions.id', 'bank_accounts.account_number', 'bank_accounts.account_name', 'value_date', 'transaction_amount',
            'transaction_narrative', 'transaction_types.name as transaction_type', 'currencies.name as currency',
        )

    return selectRes
}

let bankmodel={
    getAccountList,
    getAccountTransactions,
}

module.exports = bankmodel;
